# Dye #

A Pet Project to experiment with different libraries for byte-code generation
The same functionality is implement with

* ASM
* Javassist
* ByteBuddy

### Functionality ###
The library lets you record property paths.


```
#!java
// Create a Recorder for the type you want to create property paths for
Recorder<Person> recorder = Recorder.of(Person.class);

// use the record method to provide a single Function<> that will be used to record the property path
PathDescriptor<Person, String> path = recorder.record(p -> p.getAddress().getStreet());

// Prints [getAddress, getStreet]
System.out.println(Arrays.toString(path.getPath()));


```