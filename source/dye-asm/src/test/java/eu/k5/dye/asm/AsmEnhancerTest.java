package eu.k5.dye.asm;

import java.util.Arrays;

import org.junit.Test;

import eu.k5.dye.AbstractPerformanceTest.Example;
import eu.k5.dye.PathDescriptor;
import eu.k5.dye.Recorder;

public class AsmEnhancerTest {

	@Test
	public void basicTest() {
		Recorder<Example> recorder = Recorder.of(Example.class);
		
		PathDescriptor<Example, String> path = recorder.record(Example::getField);
		System.out.println(path);
		System.out.println(Arrays.asList(path.getPath()));

	}

}
