package eu.k5.dye.asm;

public class Foo<A> {

	private A value;

	public Foo(A value) {
		this.value = value;
	}

	public A getValue() {
		return value;
	}
}
