package eu.k5.dye.asm;

import java.util.Arrays;

import eu.k5.dye.AbstractGenericTypeTest;
import eu.k5.dye.PathDescriptor;
import eu.k5.dye.Recorder;


public class AsmGenericTest extends AbstractGenericTypeTest {
	public static void main(String[] args) {

		
		Recorder<Main> recorder = Recorder.of(Main.class);
		{
			PathDescriptor<Main, Integer> rec = recorder.record(m -> m.getGenericField().getValue());
			System.out.println(Arrays.asList(rec.getPath()));
		}
		{

			PathDescriptor<Main, Object> rec = recorder.record(m -> {
				Generic recursiveField = (Generic) m.getRecursiveField();
				Generic value = (Generic) recursiveField.getValue();
				return value.getValue();
			});
			System.out.println(Arrays.asList(rec.getPath()));
		}
	}
}
