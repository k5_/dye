package eu.k5.dye;

import java.util.Arrays;
import java.util.List;

import eu.k5.dye.PathDescriptor;
import eu.k5.dye.Recorder;
import eu.k5.dye.demo.Person;

public class Demo {
	public static void main(String[] args) {

		Recorder<Person> recorder = Recorder.of(Person.class);

		PathDescriptor<Person, String> path = recorder.record(p -> p.getAdress().getStreet());

		
		
		System.out.println(Arrays.toString(path.getPath()));
		System.out.println(path.getBaseType().getName());
		System.out.println(path.getTagetType().getName());
	
		
		List<PathDescriptor<Person, ?>> all = recorder.recordAll(p -> {
			p.getName();
			p.getAdress().getCity();
			p.getAdress().getLand();
		});
		
		System.out.println(all);
	}
	
	
}
