package eu.k5.dye.asm;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.TypeVariable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.signature.SignatureVisitor;
import org.objectweb.asm.signature.SignatureWriter;

import eu.k5.dye.BeanEnhancer;
import eu.k5.dye.TracerTypeBuilder;
import eu.k5.dye.EnhancementException;
import eu.k5.dye.Recorder;
import eu.k5.dye.Tracer;
import eu.k5.dye.TracerFactory;
import eu.k5.dye.TracerType;

public class AsmEnhancer implements BeanEnhancer {

	private static final String TRACER_FIELD_NAME = "tracer";
	private static final String MAIN_FIELD_NAME = "main";

	private static final Map<Class<?>, Class<?>> WRAPPERS;

	static {
		Map<Class<?>, Class<?>> wrappers = new HashMap<>();
		wrappers.put(int.class, Integer.class);
		wrappers.put(short.class, Short.class);
		wrappers.put(byte.class, Byte.class);
		wrappers.put(float.class, Float.class);
		wrappers.put(double.class, Double.class);
		wrappers.put(char.class, Character.class);
		wrappers.put(boolean.class, Boolean.class);
		wrappers.put(long.class, Long.class);

		WRAPPERS = Collections.unmodifiableMap(wrappers);
	}

	@Override
	public TracerFactory<?> enhance(TracerType type) {
		return createClass(type);
	}

	@Override
	public int priority() {
		return 0;
	}

	private TracerFactory<?> createClass(TracerType baseType) {

		String className = Type.getInternalName(baseType.getRawType()) + "AsmTracer" + baseType.getParameterString();
		if (className.startsWith("java/lang")) {
			className = "asm/" + className;
		}
		ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES & ClassWriter.COMPUTE_MAXS);

		String genericSupertypeDescriptor = null;
		if (baseType.isGeneric()) {
			genericSupertypeDescriptor = createSupertypeDescriptor(baseType);
		}
		cw.visit(Opcodes.V1_8, Opcodes.ACC_PUBLIC, className, genericSupertypeDescriptor,
				Type.getInternalName(baseType.getRawType()), new String[] {});

		Map<String, java.lang.reflect.Type> variableBinding = baseType.getVariableMap();

		addConstructor(cw, baseType, className);

		for (Method m : baseType.getRawType().getMethods()) {
			if (!Modifier.isFinal(m.getModifiers()) && isAccessor(m)) {
				addMethod(m, cw, className, variableBinding);
			}
		}

		cw.visitEnd();

		byte[] b = cw.toByteArray();

		try {
			Files.write(Paths.get(
					"d:/test" + baseType.getRawType().getSimpleName() + baseType.getParameterString() + ".class"), b);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Class<?> cls = new MyClassLoader().defineClass(className.replaceAll("/", "."), b);

		try {
			Constructor<?> constructor = cls.getConstructor(Tracer.class, boolean.class);
			return (tracer, main) -> newInstance(constructor, tracer, main);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new EnhancementException(e.getMessage(), e);

		}

	}

	private String createSupertypeDescriptor(TracerType type) {
		SignatureVisitor sv = new SignatureWriter();
		SignatureVisitor psv = sv.visitSuperclass();
		visitTracerTypeSignature(psv, type);
		psv.visitEnd();
		return sv.toString();
	}

	private void visitTracerTypeSignature(SignatureVisitor parent, TracerType type) {
		parent.visitClassType(Type.getInternalName(type.getRawType()));
		if (type.isGeneric()) {
			for (TracerType typeParameter : type.getTypeParameters()) {
				SignatureVisitor typeArgument = parent.visitTypeArgument('=');
				visitTracerTypeSignature(typeArgument, typeParameter);
				typeArgument.visitEnd();
			}
		}
	}

	private static <T> T newInstance(Constructor<T> constructor, Tracer tracer, boolean main) {
		try {
			return constructor.newInstance(tracer, main);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new EnhancementException(e.getMessage(), e);
		}
	}

	private boolean isAccessor(Method m) {
		return m.getParameterCount() == 0 && m.getName().startsWith("get") || m.getName().startsWith("is");
	}

	private void addConstructor(ClassWriter cw, TracerType type, String typeName) {
		cw.visitField(Opcodes.ACC_PRIVATE + Opcodes.ACC_FINAL, TRACER_FIELD_NAME, Type.getDescriptor(Tracer.class),
				null, null).visitEnd();
		cw.visitField(Opcodes.ACC_PRIVATE + Opcodes.ACC_FINAL, MAIN_FIELD_NAME, "Z", null, null).visitEnd();

		MethodVisitor mv = cw.visitMethod(Opcodes.ACC_PUBLIC, "<init>", "(" + Type.getDescriptor(Tracer.class) + "Z)V",
				null, null);

		mv.visitMaxs(3, 3);

		mv.visitVarInsn(Opcodes.ALOAD, 0);
		mv.visitMethodInsn(Opcodes.INVOKESPECIAL, Type.getInternalName(type.getRawType()), "<init>", "()V", false); // call

		mv.visitVarInsn(Opcodes.ALOAD, 0);
		mv.visitVarInsn(Opcodes.ALOAD, 1);
		mv.visitFieldInsn(Opcodes.PUTFIELD, typeName, TRACER_FIELD_NAME, Type.getDescriptor(Tracer.class));

		mv.visitVarInsn(Opcodes.ALOAD, 0);
		mv.visitVarInsn(Opcodes.ILOAD, 2);

		mv.visitFieldInsn(Opcodes.PUTFIELD, typeName, MAIN_FIELD_NAME, "Z");

		mv.visitInsn(Opcodes.RETURN);
		mv.visitEnd();
	}

	private String getRawReturnSignature(TracerType tracerType) {
		return "()" + Type.getDescriptor(tracerType.getRawType());
	}

	private String getGenericReturnSignature(TracerType tracerType) {
		if (tracerType.isGeneric()) {
			return "()" + createSupertypeDescriptor(tracerType);
		} else {
			return getRawReturnSignature(tracerType);
		}
	}

	private void addMethod(Method method, ClassWriter cw, String typeName,
			Map<String, java.lang.reflect.Type> variableBinding) {
		int methodName = cw.newConst(method.getName());

		TracerType tracerType = TracerType.fromType(method.getGenericReturnType(), variableBinding);

		if (!method.getReturnType().equals(tracerType.getRawType())) {
			addBridgeMethod(method, cw, tracerType, typeName);
		}

		MethodVisitor mv = cw.visitMethod(Opcodes.ACC_PUBLIC, method.getName(), getRawReturnSignature(tracerType),
				getGenericReturnSignature(tracerType), null);
		mv.visitMaxs(4, 4);

		conditionalCallReset(mv, typeName);

		mv.visitVarInsn(Opcodes.ALOAD, 0);
		mv.visitFieldInsn(Opcodes.GETFIELD, typeName, TRACER_FIELD_NAME, Type.getDescriptor(Tracer.class));
		mv.visitVarInsn(Opcodes.LDC, methodName);
		getClassInstance(mv, method);
		mv.visitMethodInsn(Opcodes.INVOKEINTERFACE, Type.getInternalName(Tracer.class), "pushInvocation",
				"(" + Type.getDescriptor(String.class) + Type.getDescriptor(Class.class) + ")V", true);

		if (isEnhanceable(tracerType)) {

			conditionalUseCachedField(method, mv, cw, typeName, tracerType);

			createTracerType(mv, tracerType);

			mv.visitLdcInsn(Type.getObjectType(Type.getInternalName(tracerType.getRawType())));

			mv.visitVarInsn(Opcodes.ALOAD, TRACE_SLOT);

			mv.visitMethodInsn(Opcodes.INVOKESTATIC,
					Type.getInternalName(Recorder.class), "getSupplier", "(" + Type.getDescriptor(Class.class) + ""
							+ Type.getDescriptor(TracerType.class) + ")" + Type.getDescriptor(TracerFactory.class),
					false);
			mv.visitVarInsn(Opcodes.ALOAD, 0);

			mv.visitFieldInsn(Opcodes.GETFIELD, typeName, TRACER_FIELD_NAME, Type.getDescriptor(Tracer.class));
			mv.visitInsn(Opcodes.ICONST_0);
			mv.visitMethodInsn(Opcodes.INVOKEINTERFACE, Type.getInternalName(TracerFactory.class), "create",
					"(" + Type.getDescriptor(Tracer.class) + "Z)Ljava/lang/Object;", true);

			mv.visitTypeInsn(Opcodes.CHECKCAST, Type.getInternalName(tracerType.getRawType()));
			mv.visitVarInsn(Opcodes.ASTORE, 2);
			mv.visitVarInsn(Opcodes.ALOAD, 0);
			mv.visitVarInsn(Opcodes.ALOAD, 2);

			mv.visitFieldInsn(Opcodes.PUTFIELD, typeName, method.getName() + "Recorder",
					Type.getDescriptor(tracerType.getRawType()));

			mv.visitVarInsn(Opcodes.ALOAD, 2);
			mv.visitInsn(Opcodes.ARETURN);

		} else {
			visitReturnDefaultValue(mv, method);
		}
		mv.visitEnd();
	}

	private void conditionalUseCachedField(Method method, MethodVisitor mv, ClassWriter cw, String typeName,
			TracerType tracerType) {
		String fieldName = method.getName() + "Recorder";

		cw.visitField(Opcodes.ACC_PRIVATE, fieldName, Type.getDescriptor(tracerType.getRawType()), null, null)
				.visitEnd();

		mv.visitVarInsn(Opcodes.ALOAD, 0);
		mv.visitFieldInsn(Opcodes.GETFIELD, typeName, fieldName, Type.getDescriptor(tracerType.getRawType()));

		Label label = new Label();
		mv.visitJumpInsn(Opcodes.IFNULL, label);

		mv.visitVarInsn(Opcodes.ALOAD, 0);
		mv.visitFieldInsn(Opcodes.GETFIELD, typeName, fieldName, Type.getDescriptor(tracerType.getRawType()));
		mv.visitInsn(Opcodes.ARETURN);

		mv.visitLabel(label);

		mv.visitFrame(Opcodes.F_SAME, 0, new Object[0], 0, new Object[] {});

	}

	private void addBridgeMethod(Method method, ClassVisitor cw, TracerType tracerType, String typeName) {

		String signature = "()" + Type.getDescriptor(method.getReturnType());

		MethodVisitor mv = cw.visitMethod(Opcodes.ACC_PUBLIC | Opcodes.ACC_SYNTHETIC, method.getName(), signature,
				signature, null);
		mv.visitMaxs(4, 4);

		mv.visitVarInsn(Opcodes.ALOAD, 0);
		mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, typeName, method.getName(), getRawReturnSignature(tracerType), false);
		mv.visitInsn(Opcodes.ARETURN);
	}

	private boolean isEnhanceable(TracerType tracerType) {
		return isEnhanceable(tracerType.getRawType());
	}

	private static final int BUILDER_SLOT = 2;
	private static final int TRACE_SLOT = 1;

	private void createTracerType(MethodVisitor mv, TracerType type) {
		mv.visitLdcInsn(Type.getObjectType(Type.getInternalName(type.getRawType())));

		mv.visitMethodInsn(Opcodes.INVOKESTATIC, Type.getInternalName(TracerType.class), "builder",
				"(" + Type.getDescriptor(Class.class) + ")" + Type.getDescriptor(TracerTypeBuilder.class), false);

		recCreateTracerType(mv, type);

		mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, Type.getInternalName(TracerTypeBuilder.class), "build",
				"()" + Type.getDescriptor(TracerType.class), false);

		mv.visitVarInsn(Opcodes.ASTORE, TRACE_SLOT);
	}

	private void recCreateTracerType(MethodVisitor mv, TracerType type) {

		if (!type.isGeneric()) {
			return;
		}
		for (TracerType parameter : type.getTypeParameters()) {

			mv.visitLdcInsn(Type.getObjectType(Type.getInternalName(parameter.getRawType())));

			mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, Type.getInternalName(TracerTypeBuilder.class), "addParam",
					"(" + Type.getDescriptor(Class.class) + ")" + Type.getDescriptor(TracerTypeBuilder.class), false);

			recCreateTracerType(mv, parameter);

			mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, Type.getInternalName(TracerTypeBuilder.class), "buildParam",
					"()" + Type.getDescriptor(TracerTypeBuilder.class), false);

		}
	}

	private void visitReturnDefaultValue(MethodVisitor mv, Method method) {
		Class<?> returnType = method.getReturnType();
		if (returnType.isPrimitive()) {
			if (float.class.equals(returnType)) {
				mv.visitInsn(Opcodes.FCONST_0);
				mv.visitInsn(Opcodes.FRETURN);
			} else if (double.class.equals(returnType)) {
				mv.visitInsn(Opcodes.DCONST_0);
				mv.visitInsn(Opcodes.DRETURN);
			} else if (long.class.equals(returnType)) {
				mv.visitInsn(Opcodes.LCONST_0);
				mv.visitInsn(Opcodes.LRETURN);
			} else {
				mv.visitInsn(Opcodes.ICONST_0);
				mv.visitInsn(Opcodes.IRETURN);
			}
		} else {
			mv.visitInsn(Opcodes.ACONST_NULL);
			mv.visitInsn(Opcodes.ARETURN);
		}

	}

	private void getClassInstance(MethodVisitor mv, Method method) {
		if (method.getReturnType().isPrimitive()) {
			getClassInstancePrimitive(mv, method.getReturnType());
		} else {
			mv.visitLdcInsn(Type.getObjectType(Type.getInternalName(method.getReturnType())));
		}
	}

	private void getClassInstancePrimitive(MethodVisitor mv, Class<?> type) {
		Class<?> wrapper = WRAPPERS.get(type);
		mv.visitFieldInsn(Opcodes.GETSTATIC, Type.getInternalName(wrapper), "TYPE", Type.getDescriptor(Class.class));
	}

	private void conditionalCallReset(MethodVisitor mv, String typeName) {
		mv.visitVarInsn(Opcodes.ALOAD, 0);

		mv.visitFieldInsn(Opcodes.GETFIELD, typeName, MAIN_FIELD_NAME, "Z");

		Label label = new Label();
		mv.visitJumpInsn(Opcodes.IFEQ, label);

		mv.visitVarInsn(Opcodes.ALOAD, 0);
		mv.visitFieldInsn(Opcodes.GETFIELD, typeName, TRACER_FIELD_NAME,
				"L" + Type.getInternalName(Tracer.class) + ";");
		mv.visitMethodInsn(Opcodes.INVOKEINTERFACE, Type.getInternalName(Tracer.class), "start", "()V", true);

		mv.visitLabel(label);

		mv.visitFrame(Opcodes.F_SAME, 0, new Object[0], 0, new Object[] {});
	}

	private boolean isEnhanceable(Class<?> type) {
		return !type.isPrimitive() && !Modifier.isFinal(type.getModifiers());
	}

	static class MyClassLoader extends ClassLoader {
		public Class<?> defineClass(String name, byte[] b) {
			return defineClass(name, b, 0, b.length);
		}
	}
}
