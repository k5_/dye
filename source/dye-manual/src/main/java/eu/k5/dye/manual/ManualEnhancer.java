package eu.k5.dye.manual;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.function.Function;

import eu.k5.dye.BeanEnhancer;
import eu.k5.dye.Tracer;
import eu.k5.dye.TracerFactory;
import eu.k5.dye.TracerType;

public class ManualEnhancer implements BeanEnhancer {

	public TracerFactory<?> enhance(TracerType type) {

		String name = type.getRawType().getName() + "Recorder" + type.getParameterString();
		Constructor<?> constructor;
		try {
			Class<?> cls = Class.forName(name);
			constructor = cls.getConstructor(Tracer.class, boolean.class);
			return (tracer, main) -> newInstance(constructor, tracer, main);
		} catch (NoSuchMethodException | SecurityException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}



	private static <T> T newInstance(Constructor<T> constructor, Tracer tracer, boolean main) {
		try {
			return constructor.newInstance(tracer,main);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int priority() {
		// TODO Auto-generated method stub
		return 0;
	}
}
