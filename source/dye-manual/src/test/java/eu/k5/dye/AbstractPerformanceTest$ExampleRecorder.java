package eu.k5.dye;

import eu.k5.dye.AbstractPerformanceTest.Example;
import eu.k5.dye.AbstractPerformanceTest;
import eu.k5.dye.Recorder;
import eu.k5.dye.Tracer;

public class AbstractPerformanceTest$ExampleRecorder extends Example {

	private final boolean main;

	private final Tracer tracer;

	private Example ex;

	public AbstractPerformanceTest$ExampleRecorder(Tracer tracer, boolean main) {
		this.tracer = tracer;
		this.main = main;
	}

	@Override
	public Example getEx() {
		if (main) {
			tracer.start();
		}
		tracer.pushInvocation("ex", Example.class);
		if (ex == null) {
			ex = Recorder.getSupplier(Example.class).create(tracer, false);
		}
		return ex;
	}

	@Override
	public String getField() {
		if (main) {
			tracer.start();
		}
		tracer.pushInvocation("field", String.class);
		return null;
	}
}
