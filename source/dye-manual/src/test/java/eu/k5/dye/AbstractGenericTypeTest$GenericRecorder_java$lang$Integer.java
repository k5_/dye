package eu.k5.dye;

import eu.k5.dye.AbstractGenericTypeTest.Generic;
import eu.k5.dye.Tracer;

public class AbstractGenericTypeTest$GenericRecorder_java$lang$Integer extends Generic<Integer> {
	private Tracer tracer;
	private boolean main;
	
	
	public AbstractGenericTypeTest$GenericRecorder_java$lang$Integer(Tracer tracer, boolean main) {
		this.tracer = tracer;
		this.main = main;
	}
	
	
	@Override
	public Integer getValue() {
		tracer.pushInvocation("getValue", Integer.class);
		return null;
	}

}
	