package eu.k5.dye;

import java.util.Arrays;

import eu.k5.dye.AbstractGenericTypeTest;
import eu.k5.dye.PathDescriptor;
import eu.k5.dye.Recorder;

public class ManualGenericTest extends AbstractGenericTypeTest {
	public static void main(String[] args) {

		Recorder<Main> recorder = Recorder.of(Main.class);
		{
			PathDescriptor<Main, Integer> rec = recorder.record(m -> m.getGenericField().getValue());
			System.out.println(Arrays.asList(rec.getPath()));
		}
		{

			PathDescriptor<Main, String> rec = recorder.record(m -> m.getRecursiveField().getValue().getValue());
			System.out.println(Arrays.asList(rec.getPath()));
		}
	}
}
