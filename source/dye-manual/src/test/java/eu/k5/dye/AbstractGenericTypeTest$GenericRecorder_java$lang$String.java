package eu.k5.dye;

import eu.k5.dye.AbstractGenericTypeTest.Generic;
import eu.k5.dye.Tracer;

public class AbstractGenericTypeTest$GenericRecorder_java$lang$String extends Generic<String> {
	private Tracer tracer;
	private boolean main;
	
	
	public AbstractGenericTypeTest$GenericRecorder_java$lang$String(Tracer tracer, boolean main) {
		this.tracer = tracer;
		this.main = main;
	}
	
	
	@Override
	public String getValue() {
		tracer.pushInvocation("getValue", Integer.class);
		return null;
	}
}
