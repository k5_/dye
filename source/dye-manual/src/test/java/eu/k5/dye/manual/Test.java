package eu.k5.dye.manual;

import java.util.ArrayList;
import java.util.List;

import eu.k5.dye.AbstractPerformanceTest.Example;
import eu.k5.dye.PathDescriptor;
import eu.k5.dye.Recorder;

public class Test {
	public static void main(String[] args) {
		Recorder<Example> recorder = Recorder.of(Example.class);

		PathDescriptor<Example, ?> path = recorder.record(e -> e.getEx().getEx().getField());

		List<PathDescriptor<Example, ?>> descriptors = new ArrayList<>(1000000);
		long before = System.currentTimeMillis();

		for (int i = 0; i < 1000000; i++) {
			PathDescriptor<Example, String> p = recorder.record(Example::getField);
			descriptors.add(p);

			// System.out.println(p);
		}
		System.out.println(System.currentTimeMillis() - before);
		System.out.println();
		//System.out.println(descriptors);
		// System.out.println(p);

		System.out.println(path);
		System.out.println(recorder.record(Example::getEx));

		System.out.println(recorder.recordAll(e -> {
			e.getEx().getEx().getField();
			e.getEx().getEx().getEx().getField();
			e.getEx();
			e.getEx().getField();
		}));
		
		
		
	}
	
	private void clearIfContainsOnlyZeros(List<Integer> list){
		for(int value:list){
			if (value !=0){
				return;
			}
		}
		list.clear();
	}
}
