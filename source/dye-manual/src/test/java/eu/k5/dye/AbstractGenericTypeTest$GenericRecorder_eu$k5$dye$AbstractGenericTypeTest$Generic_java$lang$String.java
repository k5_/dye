package eu.k5.dye;

import eu.k5.dye.AbstractGenericTypeTest.Generic;
import eu.k5.dye.Recorder;
import eu.k5.dye.Tracer;
import eu.k5.dye.TracerType;

public class AbstractGenericTypeTest$GenericRecorder_eu$k5$dye$AbstractGenericTypeTest$Generic_java$lang$String extends Generic<Generic<String>> {
	private Tracer tracer;
	private boolean main;

	public AbstractGenericTypeTest$GenericRecorder_eu$k5$dye$AbstractGenericTypeTest$Generic_java$lang$String(Tracer tracer, boolean main) {
		this.tracer = tracer;
		this.main = main;
	}

	public Generic<String> getValue() {
		tracer.pushInvocation("getValue");

		TracerType returnType = TracerType.builder(Generic.class).addBasicParameter(String.class).build();

		return Recorder.getSupplier(Generic.class, returnType).create(tracer, false);

	}
}