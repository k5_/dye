package eu.k5.dye;

import eu.k5.dye.AbstractGenericTypeTest.Generic;
import eu.k5.dye.AbstractGenericTypeTest.Main;
import eu.k5.dye.Recorder;
import eu.k5.dye.Tracer;
import eu.k5.dye.TracerType;

public class AbstractGenericTypeTest$MainRecorder extends Main {

	private Tracer tracer;

	private boolean main;

	public AbstractGenericTypeTest$MainRecorder(Tracer tracer, boolean main) {
		this.tracer = tracer;
		this.main = main;
	}

	@Override
	public Generic<Integer> getGenericField() {
		if(main){
			tracer.start();
		}
		tracer.pushInvocation("getGenericField");

		TracerType returnType = TracerType.builder(Generic.class).addBasicParameter(Integer.class).build();
		return Recorder.getSupplier(Generic.class, returnType).create(tracer, false);


	}

	@Override
	public Generic<Generic<String>> getRecursiveField() {
		if(main){
			tracer.start();
		}
		tracer.pushInvocation("getRecursiveField");
		TracerType returnType = TracerType.builder(Generic.class).addParam(Generic.class).addBasicParameter(String.class).buildParam().build();
		return Recorder.getSupplier(Generic.class, returnType).create(tracer, false);
	}
}
