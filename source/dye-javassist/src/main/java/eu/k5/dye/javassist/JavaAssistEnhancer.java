package eu.k5.dye.javassist;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.objectweb.asm.signature.SignatureVisitor;
import org.objectweb.asm.signature.SignatureWriter;

import eu.k5.dye.BeanEnhancer;
import eu.k5.dye.EnhancementException;
import eu.k5.dye.Recorder;
import eu.k5.dye.Tracer;
import eu.k5.dye.TracerFactory;
import eu.k5.dye.TracerType;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import javassist.CtNewConstructor;
import javassist.CtNewMethod;
import javassist.NotFoundException;
import javassist.bytecode.Bytecode;
import javassist.bytecode.ClassFile;
import javassist.bytecode.MethodInfo;
import javassist.bytecode.SignatureAttribute;

public class JavaAssistEnhancer implements BeanEnhancer {
	private AtomicInteger inc = new AtomicInteger();

	public TracerFactory<?> enhance(TracerType type) {
		try {
			ClassPool pool = ClassPool.getDefault();
			String className = type.getRawType().getSimpleName() + "JavassistRecorder" + type.getParameterString()
					+ inc.incrementAndGet();
			CtClass cc = pool.makeClass(className);

			Map<String, java.lang.reflect.Type> variableBinding = type.getVariableMap();

			CtClass superclass = pool.get(type.getRawType().getName());
			cc.setSuperclass(superclass);

			addConstructor(cc, className);
			for (Method m : type.getRawType().getMethods()) {
				if (isAccessor(m)) {
					addMethod(m, cc, variableBinding);
				}
			}

			if (type.isGeneric()) {
				addSignature(cc.getClassFile(), type);

			}
			@SuppressWarnings("unchecked")
			Constructor<?> constructor = cc.toClass().getConstructor(Tracer.class, boolean.class);
			return (tracer, main) -> newInstance(constructor, tracer, main);
		} catch (NoSuchMethodException | SecurityException | CannotCompileException | NotFoundException e) {
			throw new EnhancementException(e.getMessage(), e);
		}
	}

	private static <T> T newInstance(Constructor<T> constructor, Tracer tracer, boolean main) {
		try {
			return constructor.newInstance(tracer, main);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new EnhancementException(e.getMessage(), e);
		}
	}

	private void addConstructor(CtClass cc, String className) throws CannotCompileException {
		cc.addField(CtField.make("private final " + Tracer.class.getName() + " tracer;", cc));
		cc.addField(CtField.make("private final boolean main;", cc));
		cc.addConstructor(CtNewConstructor.make("public " + className + "(" + Tracer.class.getName()
				+ " tracer, boolean main){ this.tracer=tracer; this.main = main; }", cc));

	}

	private String createSupertypeDescriptor(TracerType type) {
		SignatureVisitor sv = new SignatureWriter();
		SignatureVisitor psv = sv.visitSuperclass();
		visitTracerTypeSignature(psv, type);
		psv.visitEnd();
		return sv.toString();
	}

	private void visitTracerTypeSignature(SignatureVisitor parent, TracerType type) {

		parent.visitClassType(org.objectweb.asm.Type.getInternalName(type.getRawType()));
		if (type.isGeneric()) {
			for (TracerType typeParameter : type.getTypeParameters()) {
				SignatureVisitor typeArgument = parent.visitTypeArgument('=');
				visitTracerTypeSignature(typeArgument, typeParameter);
				typeArgument.visitEnd();
			}
		}
	}

	private void addSignature(ClassFile classFile, TracerType tracerType) {

		String createSupertypeDescriptor = createSupertypeDescriptor(tracerType);
		SignatureAttribute signatureAttribute = new SignatureAttribute(classFile.getConstPool(),
				createSupertypeDescriptor);
		classFile.addAttribute(signatureAttribute);
	}

	private void addMethod(Method m, CtClass cc, Map<String, Type> variableBinding) throws CannotCompileException {
		if (Modifier.isFinal(m.getModifiers()))
			return;
		TracerType tracerType = TracerType.fromType(m.getGenericReturnType(), variableBinding);

		StringBuilder methodBody = new StringBuilder();
		methodBody.append("public ").append(tracerType.getRawType().getName()).append(" ").append(m.getName())
				.append("()").append("{");
		methodBody.append("if (main) {");
		methodBody.append("tracer.start();");
		methodBody.append("}");

		methodBody.append("tracer.pushInvocation(\"").append(m.getName()).append("\",")
				.append(m.getReturnType().getName()).append(".class);");

		if (isEnhanceable(tracerType)) {
			String fieldTracerName = m.getName() + "Tracer";
			cc.addField(CtField.make("private " + tracerType.getRawType().getName() + " " + fieldTracerName + ";", cc));

			methodBody.append("if(").append(fieldTracerName).append("==null){ ");
			methodBody.append(fieldTracerName).append(" = (").append(tracerType.getRawType().getName()).append(")");
			methodBody.append(Recorder.class.getName()).append(".getSupplier(").append(m.getReturnType().getName())
					.append(".class,");
			recreateTracerType(methodBody, tracerType);

			methodBody.append(").create(tracer, false);");
			methodBody.append("}");
			methodBody.append("return ").append(fieldTracerName).append(';');
		} else {
			methodBody.append("return ").append(defaultValue(tracerType.getRawType())).append(";");
		}
		methodBody.append("}");

		CtMethod ctMethod = CtNewMethod.make(methodBody.toString(), cc);
		ctMethod.setGenericSignature("()" + createSupertypeDescriptor(tracerType));
		cc.addMethod(ctMethod);

		if (!m.getReturnType().equals(tracerType.getRawType())) {
			addBridgeMethod(m, cc, tracerType);
		}
	}

	private void addBridgeMethod(Method m, CtClass cc, TracerType tracerType) throws CannotCompileException {
		Bytecode code = new Bytecode(cc.getClassFile().getConstPool());
		code.addAload(0);
		code.addInvokevirtual(cc, m.getName(), "()" + org.objectweb.asm.Type.getDescriptor(tracerType.getRawType()));
		code.addReturn(cc);
		code.setMaxLocals(1);
		MethodInfo minfo = new MethodInfo(cc.getClassFile().getConstPool(), m.getName(),
				"()" + org.objectweb.asm.Type.getDescriptor(m.getReturnType()));
		minfo.setCodeAttribute(code.toCodeAttribute());
		cc.getClassFile().addMethod(minfo);
	}

	private boolean isEnhanceable(TracerType tracerType) {
		return isEnhanceable(tracerType.getRawType());
	}

	private void recreateTracerType(StringBuilder methodBody, TracerType tracerType) {

		methodBody.append(TracerType.class.getName()).append(".builder(").append(tracerType.getRawType().getName())
				.append(".class)");

		recRecreateTracerType(methodBody, tracerType);
		methodBody.append(".build()");

	}

	private void recRecreateTracerType(StringBuilder methodBody, TracerType tracerType) {
		if (tracerType.isGeneric()) {
			for (TracerType parameter : tracerType.getTypeParameters()) {
				methodBody.append(".addParam(").append(parameter.getRawType().getName()).append(".class)");
				recRecreateTracerType(methodBody, parameter);
				methodBody.append(".buildParam()");
			}
		}
	}

	private boolean isEnhanceable(Class<?> type) {
		return !Modifier.isFinal(type.getModifiers());
	}

	private boolean isAccessor(Method m) {
		return m.getParameterCount() == 0 && m.getName().startsWith("get") || m.getName().startsWith("is");
	}

	private String defaultValue(Class<?> type) {
		if (!type.isPrimitive()) {
			return "null";
		}
		if (int.class.equals(type)) {
			return "0";
		} else if (boolean.class.equals(type)) {
			return "false";
		} else if (char.class.equals(type)) {
			return "'\\0'";
		} else if (byte.class.equals(type)) {
			return "0";
		} else if (short.class.equals(type)) {
			return "0";
		} else if (long.class.equals(type)) {
			return "0L";
		} else if (double.class.equals(type)) {
			return "0D";
		} else if (float.class.equals(type)) {
			return "0F";
		}

		throw new IllegalArgumentException("No default value available for " + type);
	}

	@Override
	public int priority() {
		return 0;
	}
}
