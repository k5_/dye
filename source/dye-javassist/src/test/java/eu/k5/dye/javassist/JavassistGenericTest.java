package eu.k5.dye.javassist;

import java.util.Arrays;

import org.junit.Ignore;

import eu.k5.dye.AbstractGenericTypeTest;
import eu.k5.dye.PathDescriptor;
import eu.k5.dye.Recorder;

public class JavassistGenericTest extends AbstractGenericTypeTest {
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Recorder<Main> recorder = Recorder.of(Main.class);
		{
			PathDescriptor<Main, Integer> rec = recorder.record(m -> {
				Generic<Integer> genericField = m.getGenericField();

				return genericField.getValue();
			});
			System.out.println(Arrays.asList(rec.getPath()));
		}
		{

			PathDescriptor<Main, Object> rec = recorder.record(m -> m.getRecursiveField().getValue().getValue());
			System.out.println(Arrays.asList(rec.getPath()));
		}
	}


}
