package eu.k5.dye.bytebuddy;

import java.util.Arrays;

import org.junit.Test;

import eu.k5.dye.AbstractPerformanceTest.Example;
import eu.k5.dye.PathDescriptor;
import eu.k5.dye.Recorder;

public class ByteBuddyEnhancerTest {

	@Test
	public void testField() {
		Recorder<Example> recorder = Recorder.of(Example.class);
		
		PathDescriptor<Example, String> path = recorder.record(Example::getField);
		System.out.println(path);
		System.out.println(Arrays.asList(path.getPath()));

	}
	@Test
	public void testComplex() {
		Recorder<Example> recorder = Recorder.of(Example.class);
		
		PathDescriptor<Example, Example> path = recorder.record(Example::getEx);
		System.out.println(path);
		System.out.println(Arrays.asList(path.getPath()));

	}

}
