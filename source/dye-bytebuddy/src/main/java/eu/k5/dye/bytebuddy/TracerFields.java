package eu.k5.dye.bytebuddy;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import eu.k5.dye.Tracer;

public final class TracerFields {
	public final Tracer tracer;
	public final boolean main;
	public final Map<String, Object> fields;
	public final Map<String, Type> variableBinding;

	public TracerFields(Tracer tracer, boolean main, Map<String, Type> variableBinding) {
		this.tracer = tracer;
		this.main = main;
		this.variableBinding = variableBinding;
		this.fields = new HashMap<>();
	}

}
