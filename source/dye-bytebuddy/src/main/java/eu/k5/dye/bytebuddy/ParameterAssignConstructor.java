package eu.k5.dye.bytebuddy;

import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.dynamic.scaffold.InstrumentedType;
import net.bytebuddy.implementation.Implementation;
import net.bytebuddy.implementation.bytecode.ByteCodeAppender;
import net.bytebuddy.implementation.bytecode.StackManipulation;
import net.bytebuddy.implementation.bytecode.member.FieldAccess;
import net.bytebuddy.implementation.bytecode.member.MethodInvocation;
import net.bytebuddy.implementation.bytecode.member.MethodReturn;
import net.bytebuddy.implementation.bytecode.member.MethodVariableAccess;
import net.bytebuddy.jar.asm.MethodVisitor;
import net.bytebuddy.matcher.ElementMatchers;

class ParameterAssignConstructor implements Implementation {

	private final Class<?> supertype;
	private final String field;

	public ParameterAssignConstructor(Class<?> supertype, String field) {
		this.supertype = supertype;
		this.field = field;
	}

	@Override
	public InstrumentedType prepare(InstrumentedType instrumentedType) {
		return instrumentedType;
	}

	@Override
	public ByteCodeAppender appender(final Target instrumentationTarget) {
		return new ByteCodeAppender() {

			@Override
			public Size apply(MethodVisitor methodVisitor, Context instrumentationContext,
					MethodDescription instrumentedMethod) {

				StackManipulation.Size size =
						new StackManipulation.Compound(MethodVariableAccess.REFERENCE.loadFrom(0),
								MethodInvocation
										.invoke(new TypeDescription.ForLoadedType(supertype).getDeclaredMethods()
												.filter(ElementMatchers.isConstructor()
														.and(ElementMatchers.takesArguments(0)))
												.getOnly()),

								MethodVariableAccess.REFERENCE.loadFrom(0), //
								MethodVariableAccess.REFERENCE.loadFrom(1), //
								FieldAccess.forField(instrumentationTarget.getInstrumentedType().getDeclaredFields()
										.filter(ElementMatchers.named(field)).getOnly()).write(),
								MethodReturn.VOID).apply(methodVisitor, instrumentationContext);
				return new Size(size.getMaximalSize(), instrumentedMethod.getStackSize());
			}
		};
	}

}