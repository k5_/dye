package eu.k5.dye.bytebuddy;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

import eu.k5.dye.Recorder;
import eu.k5.dye.TracerType;
import net.bytebuddy.implementation.bind.annotation.BindingPriority;
import net.bytebuddy.implementation.bind.annotation.FieldValue;
import net.bytebuddy.implementation.bind.annotation.Origin;
import net.bytebuddy.implementation.bind.annotation.RuntimeType;

public class Interceptor {

	@BindingPriority(0)
	@RuntimeType
	public static Object intercept(@Origin Method origin, @FieldValue("tracerFields") TracerFields fields) {
		if (fields.main) {
			fields.tracer.start();
		}
		fields.tracer.pushInvocation(origin.getName());
		if (origin.getReturnType().isPrimitive()) {
			return getDefaultValue(origin.getReturnType());
		}
		if (!fields.fields.containsKey(origin.getName())) {
			if (Modifier.isFinal(origin.getReturnType().getModifiers())) {
				fields.fields.put(origin.getName(), null);
				return null;
			}

			Type returnType = origin.getGenericReturnType();
			TracerType tracerType = TracerType.fromType(returnType, fields.variableBinding);
			if (Modifier.isFinal(tracerType.getRawType().getModifiers())) {
				fields.fields.put(origin.getName(), null);
				return null;
			}
			Object recorder = Recorder.getSupplier(tracerType.getRawType(), tracerType).create(fields.tracer, false);
			fields.fields.put(origin.getName(), recorder);
			return recorder;
		}

		return fields.fields.get(origin.getName());

	}

	private static Object getDefaultValue(Class<?> type) {
		if (!type.isPrimitive()) {
			return null;
		}
		if (int.class.equals(type)) {
			return 0;
		} else if (boolean.class.equals(type)) {
			return Boolean.FALSE;
		} else if (char.class.equals(type)) {
			return (char) 0;
		} else if (byte.class.equals(type)) {
			return (byte) 0;
		} else if (short.class.equals(type)) {
			return (short) 0;
		} else if (long.class.equals(type)) {
			return 0L;
		} else if (double.class.equals(type)) {
			return 0d;
		} else if (float.class.equals(type)) {
			return 0f;
		}

		throw new IllegalArgumentException("No default value available for " + type);
	}

}