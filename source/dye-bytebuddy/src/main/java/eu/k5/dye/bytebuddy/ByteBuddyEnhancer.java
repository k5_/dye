package eu.k5.dye.bytebuddy;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.k5.dye.BeanEnhancer;
import eu.k5.dye.EnhancementException;
import eu.k5.dye.Tracer;
import eu.k5.dye.TracerFactory;
import eu.k5.dye.TracerType;
import net.bytebuddy.ByteBuddy;
import net.bytebuddy.description.modifier.FieldManifestation;
import net.bytebuddy.description.modifier.Visibility;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.description.type.TypeDescription.Generic;
import net.bytebuddy.dynamic.DynamicType.Builder;
import net.bytebuddy.dynamic.loading.ClassLoadingStrategy;
import net.bytebuddy.implementation.MethodDelegation;
import net.bytebuddy.matcher.ElementMatchers;

public class ByteBuddyEnhancer implements BeanEnhancer {

	public TracerFactory<?> enhance(TracerType type) {
		return createClass(type);
	}

	@Override
	public int priority() {
		return 0;
	}

	private TracerFactory<?> createClass(TracerType type) {
		String className = type.getRawType().getName() + "BBTracer";

		Builder<?> classDefiner;

		if (type.isGeneric()) {
			classDefiner = new ByteBuddy().subclass(getTypeDescriptor(type));
		} else {
			classDefiner = new ByteBuddy().subclass(type.getRawType());
		}

		classDefiner = classDefiner.name(className + type.getParameterString())
				.defineField("tracerFields", TracerFields.class, Visibility.PUBLIC, FieldManifestation.FINAL) //
				.defineConstructor(1) //
				.withParameter(TracerFields.class)
				.intercept(new ParameterAssignConstructor(type.getRawType(), "tracerFields"))
				.method(ElementMatchers.nameStartsWith("get").or(ElementMatchers.nameStartsWith("is")))
				.intercept(MethodDelegation.to(Interceptor.class));

		Class<?> loaded = classDefiner.make()
				.load(ByteBuddyEnhancer.class.getClassLoader(), ClassLoadingStrategy.Default.INJECTION).getLoaded();

		try {
			Constructor<?> constructor = loaded.getConstructor(TracerFields.class);
			return (tracer, main) -> newInstance(constructor, type, tracer, main);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new EnhancementException(e.getMessage(), e);

		}

	}

	private Generic getTypeDescriptor(TracerType type) {
		return TypeDescription.Generic.Builder
				.parameterizedType(type.getRawType(), getTypeDescriptions(type.getTypeParameters())).build();
	}

	private List<Type> getTypeDescriptions(TracerType[] tracerTypes) {
		List<Type> types = new ArrayList<>(tracerTypes.length);
		for (TracerType type : tracerTypes) {
			types.add(type.asType());
		}
		return types;
	}

	private static <T> T newInstance(Constructor<? extends T> constructor, TracerType type, Tracer tracer,
			boolean main) {
		try {

			Map<String, Type> variableBinding;
			if (type.getRawType().getTypeParameters().length > 0) {
				variableBinding = new HashMap<>();
				TypeVariable<?>[] typeParameters = type.getRawType().getTypeParameters();
				for (int index = 0; index < typeParameters.length; index++) {
					variableBinding.put(typeParameters[index].getName(), type.getTypeParameters()[index].asType());
				}
			} else {
				variableBinding = Collections.emptyMap();
			}

			TracerFields fields = new TracerFields(tracer, main, variableBinding);
			return constructor.newInstance(fields);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new EnhancementException(e.getMessage(), e);
		}
	}

}
