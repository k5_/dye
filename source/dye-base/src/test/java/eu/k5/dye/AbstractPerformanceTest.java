package eu.k5.dye;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import eu.k5.dye.PathDescriptor;
import eu.k5.dye.Recorder;

public abstract class AbstractPerformanceTest {
	public static class Example {
		private Example ex;
		private String field;

		public Example getEx() {
			return ex;
		}

		public void setEx(Example ex) {
			this.ex = ex;
		}

		public String getField() {
			return field;
		}

		public void setField(String field) {
			this.field = field;
		}

	}

	private static final int WARMUP = 1000;
	private static final int RECORDS = 1_000_000;

	private static final int MEASUREMENTS = 9;

	@Test
	public void record1_000_000_fields() {

		Recorder<Example> recorder = Recorder.of(Example.class);
		warmupFields(recorder);

		List<Long> measurements = new ArrayList<>();
		for (int measurement = 0; measurement < MEASUREMENTS; measurement++) {
			measurements.add(meassureFields(recorder, RECORDS));
		}
		Collections.sort(measurements);
		System.out.println(measurements);
	}

	private long meassureFields(Recorder<Example> recorder, int records) {
		List<PathDescriptor<Example, ?>> descriptors = new ArrayList<>(records + 1);
		long before = System.currentTimeMillis();
		for (int i = 0; i < records; i++) {
			descriptors.add(recordField(recorder));
		}
		long after = System.currentTimeMillis();
		System.out.printf("Time to record %s fields: %s ms%n", records, after - before);
		return after - before;
	}

	private PathDescriptor<Example, String> recordField(Recorder<Example> recorder) {
		return recorder.record(Example::getField);
	}

	private void warmupFields(Recorder<Example> recorder) {
		for (int i = 0; i < WARMUP; i++) {
			recordField(recorder);
		}
	}

	@Test
	public void record1_000_000_paths() {
		Recorder<Example> recorder = Recorder.of(Example.class);

		warmupPaths(recorder);

		List<Long> measurements = new ArrayList<>();
		for (int measurement = 0; measurement < MEASUREMENTS; measurement++) {
			measurements.add(meassurePaths(recorder, RECORDS));
		}
		Collections.sort(measurements);
		System.out.println(measurements);
	}

	private long meassurePaths(Recorder<Example> recorder, int records) {
		List<PathDescriptor<Example, ?>> descriptors = new ArrayList<>(records + 1);

		long before = System.currentTimeMillis();
		for (int i = 0; i < records; i++) {
			descriptors.add(recordPath(recorder));
		}
		long after = System.currentTimeMillis();
		System.out.printf("Time to record %s 3 step paths: %s ms%n", records, after - before);

		return after - before;
	}

	private void warmupPaths(Recorder<Example> recorder) {
		for (int i = 0; i < WARMUP; i++) {
			recordPath(recorder);
		}
	}

	private PathDescriptor<Example, String> recordPath(Recorder<Example> recorder) {
		return recorder.record(e -> e.getEx().getEx().getField());
	}

}
