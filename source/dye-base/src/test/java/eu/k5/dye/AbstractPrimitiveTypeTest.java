package eu.k5.dye;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public abstract class AbstractPrimitiveTypeTest {

	public static class IntType {
		private int value;

		public void setValue(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	public static class ByteType {
		private byte value;

		public void setValue(byte value) {
			this.value = value;
		}

		public byte getValue() {
			return value;
		}
	}

	public static class ShortType {
		private short value;

		public void setValue(short value) {
			this.value = value;
		}

		public short getValue() {
			return value;
		}
	}

	public static class LongType {
		private long value;

		public void setValue(long value) {
			this.value = value;
		}

		public long getValue() {
			return value;
		}
	}

	public static class FloatType {
		private float value;

		public void setValue(float value) {
			this.value = value;
		}

		public float getValue() {
			return value;
		}
	}

	public static class DoubleType {
		private double value;

		public void setValue(double value) {
			this.value = value;
		}

		public double getValue() {
			return value;
		}
	}

	public static class CharType {
		private char value;

		public void setValue(char value) {
			this.value = value;
		}

		public char getValue() {
			return value;
		}
	}

	public static class BooleanType {
		private boolean value;

		public void setValue(boolean value) {
			this.value = value;
		}

		public boolean getValue() {
			return value;
		}
	}

	public <T> Recorder<T> createRecorder(Class<T> type) {
		return Recorder.of(type);
	}

	@Test
	public void byteType() {
		PathDescriptor<ByteType, Byte> rec = createRecorder(ByteType.class).record(ByteType::getValue);
		assertPath(rec);

	}

	@Test
	public void shortType() {
		PathDescriptor<ShortType, Short> rec = createRecorder(ShortType.class).record(ShortType::getValue);
		assertPath(rec);
	}

	private void assertPath(PathDescriptor<?, ?> rec) {
		List<String> actualPath = Arrays.asList(rec.getPath());
		Assert.assertEquals(Arrays.asList("getValue"), actualPath);
	}

	@Test
	public void intType() {
		PathDescriptor<IntType, Integer> rec = createRecorder(IntType.class).record(IntType::getValue);
		assertPath(rec);

	}

	@Test
	public void longType() {
		PathDescriptor<LongType, Long> rec = createRecorder(LongType.class).record(LongType::getValue);

		assertPath(rec);

	}

	@Test
	public void floatType() {
		PathDescriptor<FloatType, Float> rec = createRecorder(FloatType.class).record(FloatType::getValue);

		assertPath(rec);
	}

	@Test
	public void doubleType() {
		PathDescriptor<DoubleType, Double> rec = createRecorder(DoubleType.class).record(DoubleType::getValue);

		assertPath(rec);

	}

	@Test
	public void charType() {
		PathDescriptor<CharType, Character> rec = createRecorder(CharType.class).record(CharType::getValue);
		assertPath(rec);

	}

	@Test
	public void booleanType() {
		PathDescriptor<BooleanType, Boolean> rec = createRecorder(BooleanType.class).record(BooleanType::getValue);
		assertPath(rec);
	}

}
