package eu.k5.dye;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

public class TracerTypeTest {

	@Test
	public void basicClass() {
		TracerType type = TracerType.builder(Integer.class).build();
		assertEquals(Integer.class, type.getRawType());

		assertNull(type.getTypeParameters());
	}

	@Test
	public void missingTypeParameter() {
		try {
			TracerType.builder(List.class).build();
		} catch (IllegalStateException e) {
			assertTrue(e.getMessage(), e.getMessage().contains("1"));
			assertTrue(e.getMessage(), e.getMessage().contains("0"));
		}
	}

	@Test
	public void toManyTypeParameter() {
		try {
			TracerType.builder(List.class).addParam(Integer.class).buildParam().addParam(Integer.class).buildParam()
					.build();
		} catch (IllegalStateException e) {
			assertTrue(e.getMessage(), e.getMessage().contains("1"));
			assertTrue(e.getMessage(), e.getMessage().contains("2"));
		}
	}

	@Test
	public void test() {

		TracerType build = TracerType.builder(List.class).addBasicParameter(Integer.class).build();

	}

	@Test
	public void getParameterString() {
		TracerType build = TracerType.builder(List.class).addBasicParameter(Integer.class).build();
		assertEquals("_java$lang$Integer", build.getParameterString());
	}

}
