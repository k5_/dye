package eu.k5.dye;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;

public abstract class AbstractGenericTypeTest {
	public static class Generic<T> {

		private T value;

		public void setValue(T value) {
			this.value = value;
		}

		public T getValue() {
			return value;
		}
	}

	public static class Main {

		private Generic<Integer> genericField;

		private Generic<Generic<String>> recursiveField;

		public void setGenericField(Generic<Integer> genericField) {
			this.genericField = genericField;
		}

		public Generic<Integer> getGenericField() {
			return genericField;
		}

		public Generic<Generic<String>> getRecursiveField() {
			return recursiveField;
		}

		public void setRecursiveField(Generic<Generic<String>> recursiveField) {
			this.recursiveField = recursiveField;
		}
	}

	@Test
	public void simple() {
		Recorder<Main> recorder = Recorder.of(Main.class);
		PathDescriptor<Main, Integer> rec = recorder.record(m -> m.getGenericField().getValue());
		List<String> path = Arrays.asList(rec.getPath());
		assertEquals(Arrays.asList("getGenericField", "getValue"), path);

	}

	@Test
	public void recursiveGenerics() {
		Recorder<Main> recorder = Recorder.of(Main.class);
		PathDescriptor<Main, String> rec = recorder.record(m -> m.getRecursiveField().getValue().getValue());
		List<String> path = Arrays.asList(rec.getPath());
		assertEquals(Arrays.asList("getRecursiveField", "getValue", "getValue"), path);
	}

	@Test
	public void genericClassSignature() {
		Recorder<Main> recorder = Recorder.of(Main.class);

		recorder.record(m -> {
			Generic<Integer> field = m.getGenericField();

			Type superclass = field.getClass().getGenericSuperclass();
			Assert.assertTrue("Class extends raw type", superclass instanceof ParameterizedType);
			Assert.assertEquals(((ParameterizedType) superclass).getActualTypeArguments()[0], Integer.class);

			return field.getValue();
		});
	}

	@Test
	public void genericMethodSignature() {
		Recorder<Main> recorder = Recorder.of(Main.class);

		recorder.record(m -> {

			try {
				Method method = m.getClass().getMethod("getGenericField");
				Type returnType = method.getGenericReturnType();
				Assert.assertTrue("Return type has no generic signature", returnType instanceof ParameterizedType);
				Assert.assertEquals(((ParameterizedType) returnType).getActualTypeArguments()[0], Integer.class);

			} catch (NoSuchMethodException | SecurityException e) {
				Assert.fail(e.getMessage());
			}
			return null;
		});
	}

	@Test
	public void bridgeMethods() {
		Recorder<Main> recorder = Recorder.of(Main.class);
		PathDescriptor<Main, Integer> rec = recorder.record(m -> {
			Generic<Integer> field = m.getGenericField();

			Method bridgedGetter = null;
			try {
				List<Method> methods = Arrays.stream(field.getClass().getMethods())
						.filter(method -> method.getName().equals("getValue"))
						.filter(method -> method.getReturnType().equals(Integer.class)) //
						.collect(Collectors.toList());

				Assert.assertEquals("No Integer specific method created: ", methods.size(), 1);
				bridgedGetter = methods.get(0);
			} catch (SecurityException e) {
				Assert.fail("No Integer specific method created: " + e.getMessage());
			}

			try {
				return (Integer) bridgedGetter.invoke(field);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				throw new RuntimeException(e);
			}
		});

	}

	@Test
	public void rawCasts() {
		Recorder<Main> recorder = Recorder.of(Main.class);

		PathDescriptor<Main, Object> rec = recorder.record(m -> {
			Generic recursiveField = (Generic) m.getRecursiveField();
			Generic value = (Generic) recursiveField.getValue();
			return value.getValue();
		});
		List<String> path = Arrays.asList(rec.getPath());
		assertEquals(Arrays.asList("getRecursiveField", "getValue", "getValue"), path);

	}
}
