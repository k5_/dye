package eu.k5.dye;

import java.util.ArrayList;
import java.util.List;

public class PathDescriptorBuilder<T> {
	private Class<T> baseType;
	private Class<?> targetType;
	private List<String> path = new ArrayList<>(1);
	
	public PathDescriptorBuilder(Class<T> baseType) {
		this.baseType = baseType;
	}
	
	public void add(String property, Class<?> type) {
		path.add(property);
		targetType = type;
	}

	public void reset() {
		targetType = null;
		path.clear();
	}

	public PathDescriptor<T, ?> build() {
		return new PathDescriptor<>(baseType, targetType, path.toArray(new String[path.size()]));
	}

	public boolean isEmpty() {
		return targetType == null;
	}


}
