package eu.k5.dye;

public class EnhancementException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public EnhancementException(String message, Throwable cause) {
		super(message, cause);
	}

}
