package eu.k5.dye;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PropertyPathRecorder<T> implements Tracer {

	private List<PathDescriptor<T, ?>> stack;

	private PathDescriptorBuilder<T> path;

	public PropertyPathRecorder(Class<T> type) {
		path = new PathDescriptorBuilder<>(type);
	}

	public void pushInvocation(String property, Class<?> type) {
		path.add(property, type);
	}

	public void reset() {
		stack = null;
		path.reset();
	}

	public PathDescriptor<T, ?> getPath() {
		return path.build();
	}

	public List<PathDescriptor<T, ?>> getPaths() {
		if (path.isEmpty())
			return Collections.emptyList();
		List<PathDescriptor<T, ?>> paths;
		if (stack == null) {
			paths = new ArrayList<>(1);

		} else {
			paths = new ArrayList<>(stack);
		}
		paths.add(path.build());
		return paths;
	}

	@Override
	public void start() {
		if (!path.isEmpty()) {
			if (stack == null) {
				stack = new ArrayList<>(1);
			}
			stack.add(path.build());
			path.reset();
		}
	}

	@Override
	public void pushInvocation(String string) {
		pushInvocation(string, null);
	}

}
