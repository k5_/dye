package eu.k5.dye;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.ServiceLoader;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Consumer;
import java.util.function.Function;

public class Recorder<T> {
	@SuppressWarnings("unused")
	private Class<T> type;

	private PropertyPathRecorder<T> tracer;
	private T recorder;

	public Recorder(Class<T> type, T recorder, PropertyPathRecorder<T> tracer) {
		this.type = type;
		this.recorder = recorder;
		this.tracer = tracer;
	}



	@SuppressWarnings("unchecked")
	public <B> PathDescriptor<T, B> record(Function<T, B> path) {
		tracer.reset();
		path.apply(recorder);
		return (PathDescriptor<T, B>) tracer.getPath();
	}

	public List<PathDescriptor<T, ?>> recordAll(Consumer<T> path) {
		tracer.reset();
		path.accept(recorder);
		return tracer.getPaths();
	}

	private static final ConcurrentMap<TracerType, TracerFactory<?>> TRACERS = new ConcurrentHashMap<>();

	private enum InstanceHolder {
		INSTANCE;

		private final BeanEnhancer enhancer;

		private InstanceHolder() {
			ServiceLoader<BeanEnhancer> enhancer = ServiceLoader.load(BeanEnhancer.class);

			this.enhancer = enhancer.iterator().next();
		}

		public BeanEnhancer getEnhancer() {
			return enhancer;
		}
	}

	private static BeanEnhancer getEnhancer() {
		return InstanceHolder.INSTANCE.getEnhancer();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> TracerFactory<T> getSupplier(Class<T> type) {
		TracerType tracerType = new TracerType(type, null);

		return (TracerFactory<T>) (TracerFactory) TRACERS.computeIfAbsent(tracerType, t -> {
			return (TracerFactory<?>) (TracerFactory) getEnhancer().enhance(t);
		});
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <T> TracerFactory<T> getSupplier(Class<T> rawType, TracerType type) {
		return (TracerFactory<T>) (TracerFactory) TRACERS.computeIfAbsent(type, t -> {
			return (TracerFactory<?>) (TracerFactory) getEnhancer().enhance(type);
		});
	}

	private static <T> T loadRecorder(Class<T> type, Tracer rec) {
		return getSupplier(type).create(rec, true);
	}

	public static <T> Recorder<T> of(Class<T> type) {
		PropertyPathRecorder<T> rec = new PropertyPathRecorder<>(type);
		T recorder = loadRecorder(type, rec);
		return new Recorder<>(type, recorder, rec);
	}
}
