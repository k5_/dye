package eu.k5.dye;

import java.util.Arrays;

public class PathDescriptor<B, T> {

	private Class<B> baseType;
	private Class<T> tagetType;

	private String[] path;

	public PathDescriptor(Class<B> baseType, Class<T> targetType, String[] path) {
		this.baseType = baseType;
		this.tagetType = targetType;
		this.path = path;
	}

	public String[] getPath() {
		return path;
	}

	public Class<B> getBaseType() {
		return baseType;
	}

	public Class<T> getTagetType() {
		return tagetType;
	}

	@Override
	public String toString() {
		if (tagetType == null) {
			return baseType.getName() + Arrays.toString(path);

		} else {
			return baseType.getName() + "->" + tagetType.getName() + " " + Arrays.toString(path);
		}
	}

}
