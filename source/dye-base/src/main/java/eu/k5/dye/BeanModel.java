package eu.k5.dye;

import java.util.ArrayList;
import java.util.List;

public class BeanModel {

	private List<Getter> getters = new ArrayList<>();

	
	
	
	static class Getter {
		private final TracerType returnType;
		private final String name;

		public Getter(TracerType returnType, String name) {
			this.returnType = returnType;
			this.name = name;
		}

	}
}
