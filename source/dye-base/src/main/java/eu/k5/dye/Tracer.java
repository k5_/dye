package eu.k5.dye;

public interface Tracer {

	void pushInvocation(String string, Class<?> type);
	void pushInvocation(String string);
	
	void start();
	
}
