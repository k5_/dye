package eu.k5.dye;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class TracerType {
	private final Class<?> rawType;
	private final TracerType[] typeParameters;

	TracerType(Class<?> rawType, TracerType[] typeParameters) {
		this.rawType = rawType;
		this.typeParameters = typeParameters;
	}

	public Class<?> getRawType() {
		return rawType;
	}

	public TracerType[] getTypeParameters() {
		return typeParameters;
	}

	public Type asType() {
		if (typeParameters == null) {
			return rawType;
		}
		Type[] actualTypeParameters = new Type[typeParameters.length];
		for (int index = 0; index < typeParameters.length; index++) {
			actualTypeParameters[index] = typeParameters[index].asType();
		}
		return new TracerParameterizedType(rawType, actualTypeParameters);
	}

	public String getParameterString() {
		StringBuilder builder = new StringBuilder();
		if (typeParameters != null) {
			for (TracerType type : typeParameters) {
				builder.append("_");
				type.addParameter(builder);
			}
		}
		return builder.toString();
	}

	private void addParameter(StringBuilder builder) {
		builder.append(rawType.getCanonicalName().replaceAll(Pattern.quote("."), Matcher.quoteReplacement("$")));
		if (typeParameters != null) {
			for (TracerType type : typeParameters) {
				builder.append("_");
				type.addParameter(builder);
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rawType == null) ? 0 : rawType.hashCode());
		result = prime * result + Arrays.hashCode(typeParameters);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TracerType other = (TracerType) obj;
		if (rawType == null) {
			if (other.rawType != null)
				return false;
		} else if (!rawType.equals(other.rawType))
			return false;
		if (!Arrays.equals(typeParameters, other.typeParameters))
			return false;
		return true;
	}

	public static class TracerParameterizedType implements ParameterizedType {
		private final Class<?> rawType;
		private final Type[] actualTypeParameters;

		public TracerParameterizedType(Class<?> rawType, Type[] actualTypeParameters) {
			this.rawType = rawType;
			this.actualTypeParameters = actualTypeParameters;
		}

		@Override
		public Type[] getActualTypeArguments() {
			return actualTypeParameters;
		}

		@Override
		public Type getRawType() {
			return rawType;
		}

		@Override
		public Type getOwnerType() {
			return null;
		}

	}

	public static TracerTypeBuilder builder(Class<?> rawType) {
		return new TracerTypeBuilder(null, rawType);
	}

	public static TracerType fromType(Type returnType, Map<String, Type> variableBinding) {
		if (returnType instanceof Class<?>) {
			return new TracerType((Class<?>) returnType, null);
		} else if (returnType instanceof ParameterizedType) {

			TracerTypeBuilder builder = builder((Class<?>) ((ParameterizedType) returnType).getRawType());
			for (Type type : ((ParameterizedType) returnType).getActualTypeArguments()) {
				builder.addTypeParameter(type);
			}
			return builder.build();
		} else if (returnType instanceof TypeVariable<?>) {
			Type type = variableBinding.get(((TypeVariable<?>) returnType).getName());
			if (type == null) {
				throw new IllegalArgumentException("Unknown type: " + returnType.getClass().getName());
			}
			return fromType(type, Collections.emptyMap());
		} else {
			throw new IllegalArgumentException("Unknown type: " + returnType.getClass().getName());

		}
	}

	public boolean isGeneric() {
		return typeParameters != null && typeParameters.length > 0;
	}

	public Map<String, java.lang.reflect.Type> getVariableMap() {
		Map<String, java.lang.reflect.Type> variableBinding = new HashMap<>();
		TypeVariable<?>[] typeParameters = getRawType().getTypeParameters();
		for (int index = 0; index < typeParameters.length; index++) {
			variableBinding.put(typeParameters[index].getName(), getTypeParameters()[index].asType());
		}
		return variableBinding;
	}

}
