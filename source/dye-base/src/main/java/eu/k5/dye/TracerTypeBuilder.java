package eu.k5.dye;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class TracerTypeBuilder {

	private Class<?> rawType;
	private TracerTypeBuilder parent;

	private List<TracerType> parameters = new ArrayList<>();

	public TracerTypeBuilder(TracerTypeBuilder parent, Class<?> rawType) {
		this.parent = parent;
		this.rawType = rawType;
	}

	public TracerTypeBuilder addParam(Class<?> rawType) {
		return new TracerTypeBuilder(this, rawType);
	}

	public TracerTypeBuilder addTypeParameter(Type type){
		if (type instanceof Class<?>){
			return addBasicParameter((Class<?>)type);
		} else if (type instanceof ParameterizedType){
			TracerTypeBuilder paramBuilder = addParam((Class<?>) ((ParameterizedType) type).getRawType());
			for(Type parameter:((ParameterizedType) type).getActualTypeArguments()){
				paramBuilder.addTypeParameter(parameter);
			}
			return paramBuilder.buildParam();
		} else {
			throw new IllegalArgumentException("Unknown type: " + type.getClass().getName());
		}
	}
	
	public TracerTypeBuilder addBasicParameter(Class<?> type) {
		int expectedParameters = type.getTypeParameters().length;
		if (0 != expectedParameters) {
			throw new IllegalStateException("Type " + type.getCanonicalName() + " has " + expectedParameters
					+ " Typeparameters. Only 0 given.");
		}
		parameters.add(new TracerType(type, null));
		return this;
	}

	public TracerTypeBuilder buildParam() {
		if (parent == null) {
			throw new IllegalStateException("Builder is already on root of type tree");
		}
		parent.parameters.add(this.build());
		return parent;
	}

	public TracerType build() {
		int expectedParameters = rawType.getTypeParameters().length;
		if (parameters.size() != expectedParameters) {
			throw new IllegalStateException("Type " + rawType.getCanonicalName() + " has " + expectedParameters
					+ " Typeparameters. Only " + parameters.size() + " given.");
		}
		if (parameters.isEmpty()) {
			return new TracerType(rawType, null);
		} else {
			return new TracerType(rawType, parameters.toArray(new TracerType[parameters.size()]));
		}
	}
}