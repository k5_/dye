package eu.k5.dye;

@FunctionalInterface
public interface TracerFactory<T> {

	T create(Tracer parent, boolean main);
}
